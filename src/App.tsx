import { useDispatch, useSelector } from "react-redux";
import { RootState } from "./store/store";
import { decrement, incerement } from "./store/reducer/numberReducer";
import { changeText } from "./store/reducer/textReducer";
import { updatePlayer } from "./store/reducer/playerReducer";

function App() {

	const sate = useSelector((state : RootState) => state.number)
	const text = useSelector((state : RootState) => state.text)
	const player = useSelector((state : RootState) => state.player)
	const dispatch = useDispatch();

	return (
		<>
			<span>Value : {sate.number}</span><br />
			<button onClick={() => {dispatch(incerement())}}>+</button>
			<button onClick={() => {dispatch(decrement())}}>-</button> <br />

			<span><label htmlFor="">You Write : {text.text}</label><input type="text" name="" id="" onChange={(e) => {dispatch(changeText(e.target.value))}} /></span>

			<span>
				<p>PlayerName : {player.name}</p>
				<p>PlayerHp : {player.hp}</p>
				<p>PlayerMana : {player.mana}</p>
				<p>PlayerLevel : {player.lvl}</p>
				<button onClick={() => {dispatch(updatePlayer({name : 'alha' , hp : player.hp + 10, mana : player.mana + 10, lvl : player.lvl + 10}))}}>UPDATE PLAYER</button>
			</span>
		</>
	)	
}

export default App;
