import { configureStore } from "@reduxjs/toolkit";
import { numberSlice } from "./reducer/numberReducer";
import { textSlice } from "./reducer/textReducer";
import playerSlice from "./reducer/playerReducer";

export const store = configureStore({
	reducer : {
		number : numberSlice.reducer,
		text : textSlice.reducer,
		player : playerSlice.reducer
	}
})

export type RootState = ReturnType<typeof store.getState>;
