import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	number : 0
}

export const numberSlice = createSlice({
	name : 'asd',
	initialState,
	reducers : {
		incerement : (state) => {
			state.number += 1
		},
		decrement: (state) => {
			state.number -= 1
		}
	}
})

export const {incerement , decrement} = numberSlice.actions

export default numberSlice