import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	text : ''
}

type ActionPayloadType = {
	type : string,
	payload : string,
}

export const textSlice = createSlice({
	name : 'text',
	initialState,
	reducers : {
		changeText: (state , actions : ActionPayloadType) => {
			state.text = actions.payload
		}
	}
})

export const {changeText} = textSlice.actions
export default textSlice