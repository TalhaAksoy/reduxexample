import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	name : "Default Name",
	hp : 1,
	mana : 2,
	lvl : 3
}

type ActionPayloadType = {
	type : string,
	payload : {name : string , hp : number, mana : number, lvl : number},
}

const playerSlice = createSlice({
	name : 'player',
	initialState,
	reducers : {
		updatePlayer: (state , actions : ActionPayloadType) => {
			state.name = actions.payload.name,
			state.hp = actions.payload.hp,
			state.mana = actions.payload.mana,
			state.lvl = actions.payload.lvl
		}
	}
})

export const { updatePlayer } = playerSlice.actions
export default playerSlice